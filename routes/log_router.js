const express = require('express');
const router = express.Router();
const logController = require('../controllers/log_controller');

router.put('/:id', logController.updateLog);

module.exports = router;