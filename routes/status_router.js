const express = require('express');
const router = express.Router();
const statusController = require('../controllers/status_controller');

router.put('/:id', statusController.updateStatus);

module.exports = router;