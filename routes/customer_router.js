const express = require('express');
const router = express.Router();
const customerController = require('../controllers/customer_controller');

router.post('/', customerController.createCustomer);
router.patch('/:id', customerController.updateCustomer);

router.get('/', customerController.getCustomers);
router.get('/:id', customerController.getCustomer);

router.delete('/:id', customerController.deleteCustomer);

module.exports = router;