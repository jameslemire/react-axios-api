var customers = require('../models/customer_model');

module.exports = {
  createCustomer: (req, res) => {
    var id = customers[ customers.length - 1 ].id + 1;

    const {
      first, last, status, email, phone, log
    } = req.body;
    
    customers.push({
      id,
      first,
      last,
      status,
      email,
      phone,
      log
    })

    res.send( `Customer with with id of: ${id} has been created successfully` );
  },

  updateCustomer: (req, res) => {
    const customer = customers.find( customer => customer.id == req.params.id );
    const index = customers.findIndex( customer => customer.id == req.params.id );

    if ( !customer ) { res.status(500).send( `No customer with id of: ${req.params.id} found` ); return; }

    const {
      first, last, status, email, phone, log
    } = req.body;

    customers[index] = {
      id: customer.id,
      first: first || customer.first,
      last: last || customer.last,
      status: status || customer.status,
      email: email || customer.email,
      phone: phone || customer.phone,
      log: log || customer.log
    }

    res.send( customers[index] );
  },

  getCustomers: (req, res) => {
    setTimeout( () => res.send(customers), 1000 );
    // res.send(customers);
  },

  getCustomer: (req, res) => {
    var customer = customers.find( (customer) => customer.id == req.params.id );
    if ( customer ) {
      setTimeout( () => res.send( customer ), 1000 );
      // res.send( customer );
    } else {
      setTimeout( () => res.status(500).send( `No customer found with id of: ${req.params.id}` ), 1000 );
      // res.send( `No customer found with id of: ${req.params.id}` );
    }
  },

  deleteCustomer: (req, res) => {
    customers = customers.filter( (customer) => customer.id != req.params.id );
    res.send( `Customer with id of: ${req.params.id} has been deleted` );
  }
} 