const customers = require('../models/customer_model');

module.exports = {
  updateLog: (req, res) => {
    var customer = customers.find( customer => customer.id == req.params.id );
    var index = customers.findIndex( customer => customer.id == req.params.id );

    customers[index].log = req.body.log;
    res.send( req.body.log );
  }
}