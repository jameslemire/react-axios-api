const customers = require('../models/customer_model');

module.exports = {
  updateStatus: (req, res) => {
    console.log('Update status fired:', req.body, req.params);
    var customer = customers.find( customer => customer.id == req.params.id );
    var index = customers.findIndex( customer => customer.id == req.params.id );

    customers[index].status = req.body.status;

    res.send( req.body.status );
  }
}
