const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');
const app = express();

app.use( bodyParser.json() );
app.use( cors() );

const customerRouter = require('./routes/customer_router');
const statusRouter = require('./routes/status_router');
const logRouter = require('./routes/log_router');

app.use('/api/customer/', customerRouter);
app.use('/api/status/', statusRouter);
app.use('/api/log/', logRouter);

const port = 3005;
app.listen(port, () => console.log(`Server listening on port ${port}.`) );